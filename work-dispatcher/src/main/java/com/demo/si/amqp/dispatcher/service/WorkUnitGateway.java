package com.demo.si.amqp.dispatcher.service;

import com.demo.si.amqp.dispatcher.config.OutboundIntegrationConfig;
import com.demo.si.amqp.api.model.WorkUnit;
import org.springframework.integration.annotation.Gateway;
import org.springframework.integration.annotation.MessagingGateway;

@MessagingGateway
public interface WorkUnitGateway {
    @Gateway(requestChannel = OutboundIntegrationConfig.WORKS_CHANNEL)
    void generate(WorkUnit workUnit);

}