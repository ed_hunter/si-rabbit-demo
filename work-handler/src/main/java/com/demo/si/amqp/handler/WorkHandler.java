package com.demo.si.amqp.handler;

import com.demo.si.amqp.api.model.WorkUnit;
import lombok.extern.slf4j.Slf4j;
import org.springframework.integration.annotation.MessageEndpoint;
import org.springframework.integration.annotation.ServiceActivator;

@MessageEndpoint
@Slf4j
public class WorkHandler {
    @ServiceActivator(inputChannel = InboundIntegrationConfig.INCOMMING_WORK_CHANNEL)
    public void process(WorkUnit workUnit) {
        log.info(">>>Starting workUnit: " + workUnit.getId());
        delay(500);
        log.info(">>>workUnit: processed " + workUnit);
    }

    private void delay(int millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            // ignore
        }
    }
}
