package com.example.si.amqp.sirabbitdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SiRabbitDemoApplication {

	public static void main(String[] args) {
		SpringApplication.run(SiRabbitDemoApplication.class, args);
	}

}
