package com.demo.si.amqp.api.model;

import lombok.Data;

@Data
public class WorkUnit {
    private String id;
    private String description;
}
