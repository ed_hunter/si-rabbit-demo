package com.demo.si.amqp.dispatcher;

import lombok.extern.slf4j.Slf4j;
import com.demo.si.amqp.api.model.WorkUnit;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;
import com.demo.si.amqp.dispatcher.service.WorkUnitGateway;

import java.util.UUID;

@Component
@Slf4j
public class AppRunner implements ApplicationRunner {
    @Autowired
    private WorkUnitGateway workUnitGateway;

    @Override
    public void run(ApplicationArguments args) {
        for (int i = 0; i < 100; i++) {
            WorkUnit workUnit = new WorkUnit();
            workUnit.setId(UUID.randomUUID().toString());
            workUnit.setDescription("test#" + i);
            log.info("Sending " + workUnit);
            workUnitGateway.generate(workUnit);
        }

    }
}
