package com.demo.si.amqp.handler;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class WorkHandlerMain {
    public static void main(String[] args) {
        SpringApplication.run(WorkHandlerMain.class, args);
    }
}
